# Django CI

Lembretes para implementação continua de aplicações Django.

## Getting started

Instalar GitLab Runner no host:
``` bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
```
Registre usando o token de registro (não confunda com token de acesso).

GitLab->Projeto->Settings->CI/CD->Runners

Executor: shell 
```
sudo gitlab-runner register
```
Adicionar usuário gitlab-runner no grupo do usuário do login que executa o git. 
Em um servidor Ubuntu da AWS esse usuário é "ubuntu" no grupo "ubuntu" (ubuntu:ubuntu).
``` bash 
usermod -a -G ubuntu gitlab-runner
```
Crie um token de acesso no GitLab e clone o projeto:
User Settings -> Access Tokens
``` bash 
git clone https://<gitlab-user>:<token>@gitlab.com/<gitlab-user>/<projeto>.git
```
Instalar versão do python usado no projeto caso seja diferente do servidor.
``` bash
sudo apt install python3.10 python3.10-dev python3.10-venv -y
sudo -H pip3 install --upgrade pip
sudo -H pip3 install virtualenv
```
Criar ambiente virtual na pasta do projeto:
``` bash
virtualenv -p /usr/bin/python3.10 venv
```

~/myprojectdir/myproject/settings.py
``` python
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
```

/etc/systemd/system/gunicorn.socket
```
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target
```
/etc/systemd/system/gunicorn.service
```
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=<user>
Group=<group>
WorkingDirectory=/home/<user>/<projeto_dir>/<projeto>
ExecStart=/home/<user>/<projeto_dir>/venv/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          <projeto>.wsgi:application

[Install]
WantedBy=multi-user.target
```
``` bash
sudo systemctl start gunicorn.socket
sudo systemctl enable gunicorn.socket
```
verificar:
``` bash
sudo systemctl status gunicorn.socket
``` 
Testar:
``` bash
curl --unix-socket /run/gunicorn.sock localhost
```
Restart:
``` bash
sudo systemctl daemon-reload
sudo systemctl restart gunicorn
``` 

settings.py
``` python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'OPTIONS': {
            'service': 'projeto_service',
        },
    }
}
Criar arquivo pg_service.conf na pasta obtida com o comando:
```
pg_config --sysconfdir
```

``` 
pg_service.conf
[~/.pg_service.conf](https://www.postgresql.org/docs/current/libpq-pgservice.html)
```
[projeto_service]
host=localhost
user=USER
dbname=NAME
port=5432
```


[/home/gitlab-runner/.pgpass](https://www.postgresql.org/docs/current/libpq-pgpass.html)
```
localhost:5432:NAME:USER:PASSWORD
```
```
chmod 0600 /home/gitlab-runner/.pgpass
chown gitlab-runner:gitlab-runner /home/gitlab-runner/.pgpass
```
PostgreSQL
```
sudo -u postgres psql
```
```
CREATE DATABASE projeto;
CREATE USER projetouser WITH PASSWORD 'password';
ALTER ROLE projetouser SET client_encoding TO 'utf8';
ALTER ROLE projetouser SET default_transaction_isolation TO 'read committed';
ALTER ROLE projetouser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE projeto TO projetouser;
```
## Referências
[Como configurar o Django com o Postgres, Nginx e o Gunicorn no Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-20-04-pt)

[Installing and using virtualenv with Python 3](https://help.dreamhost.com/hc/en-us/articles/115000695551-Installing-and-using-virtualenv-with-Python-3)

